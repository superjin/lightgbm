#ifndef LIGHTGBM_ServiceImpl_HPP_
#define LIGHTGBM_ServiceImpl_HPP_

#include <LightGBM/meta.h>
#include <LightGBM/boosting.h>
#include <LightGBM/utils/lock.h>
#include <LightGBM/utils/text_reader.h>
#include <LightGBM/dataset.h>
#include <LightGBM/json11.hpp>

#include <map>
#include <cstring>
#include <cstdio>
#include <vector>
#include <utility>
#include <functional>
#include <string>
#include <memory>
#include <unistd.h>

#include "predictor.hpp"

#ifdef USE_GRPC

#include <grpc/grpc.h>
#include <grpc++/grpc++.h>
#include "../protos/model_service.grpc.pb.h"

namespace LightGBM {

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using namespace modelx;

struct ModelContainer {
  ModelContainer(Boosting *model, const std::string &name, const std::string &path, long last_modified) :
      name_(name), path_(path), last_modified_(last_modified) {
    model_.reset(model);
  }
  std::string name_;
  std::string path_;
  long last_modified_;
  std::unique_ptr<Boosting> model_;
};

class ServiceImpl final : public ModelService::Service {
 public:
  explicit ServiceImpl(const std::string &model_config) : model_config_(model_config), run_(true) {
    if (!ReloadModelConfigFile()) {
      Log::Fatal("model config cannot be load");
    }
    StartWatcher();
  }

  ~ServiceImpl() {
  }

  Status Predict(ServerContext *context, const PredictionRequest *request,
                 PredictionResponse *response) override {
    if (!request->has_model_spec() || request->model_spec().name().empty()) {
      Log::Warning("model spec not found");
      return Status(StatusCode::NOT_FOUND, "model spec not found");
    }
    std::shared_ptr<ModelContainer> model = GetModel(request->model_spec().name());

    if (!model) {
      Log::Warning("model %s not found", request->model_spec().name().c_str());
      return Status(StatusCode::UNAVAILABLE, "model " + request->model_spec().name() + "not found");
    }
    Predictor predictor(model->model_.get(), -1, false, false, false, false, 1000, 10.0);
    std::vector<std::pair<int, double>> features;
    for (auto &f: request->append_feats().feats()) {
      features.emplace_back(f.feat_id(), f.feat_val());
    }

    std::vector<double> result(1);
    for (auto &item_feats: request->feats()) {
      for (auto &f: item_feats.feats()) {
        features.emplace_back(f.feat_id(), f.feat_val());
      }
      predictor.PredictByFeatures(features, result);
      response->add_outputs(result[0]);
      features.resize(request->append_feats().feats_size());
    }
    auto model_spec = response->mutable_model_spec();
    model_spec->set_name(request->model_spec().name());
    return Status::OK;
  }

 private:
  bool ReloadModelConfigFile() {
    Log::Info("load model config from %s", model_config_.c_str());
    std::string content;
    try {
      TextReader<size_t> config_reader(model_config_.c_str(), false);
      size_t buffer_len = 0;
      auto buffer = config_reader.ReadContent(&buffer_len);
      content = std::string(buffer.data(), 0, buffer_len);
    } catch (const std::runtime_error &error) {
      Log::Warning("load model config error, file: %d, err: %s", model_config_.c_str(), error.what());
      return false;
    }
    if (content.length() == 0) {
      Log::Warning("model config has no content");
      return false;
    }

    std::string err;
    json11::Json json = json11::Json::parse(content, err);

    if (!err.empty()) {
      Log::Warning("decode model config error: %s", err.c_str());
      return false;
    }

    std::unordered_map<std::string, bool> model_name_map;
    for (auto &v: json["config"].array_items()) {
      auto name = v["name"].string_value();
      auto base_path = v["base_path"].string_value();
      if (name.empty()) {
        Log::Warning("model name is empty");
        continue;
      }
      if (base_path.empty()) {
        Log::Warning("model path is empty");
        continue;
      }
      if (!VirtualFileWriter::Exists(base_path)) {
        Log::Warning("model path %s is not exists", base_path.c_str());
        continue;
      }
      ReloadModel(name, base_path);
      model_name_map[name] = true;
    }

    if (model_name_map.size() == 0) {
      Log::Warning("model config is empty, may be field name is wrong");
      return false;
    }

    for (auto it = models_.cbegin(); it != models_.cend();) {
      if (model_name_map.find(it->first) == model_name_map.end()) {
        AutoWrite(&this->rw_lock_);
        Log::Info("unload model %s from %s, tm: %d",
                  it->first.c_str(),
                  it->second->path_.c_str(),
                  it->second->last_modified_);
        this->models_.erase(it++);
      } else {
        it++;
      }
    }
    return true;
  }

  void ReloadModel(const std::string &name, const std::string &path) {
    auto tm = VirtualFileStat::GetLastModified(path);
    if (tm <= 0) {
      Log::Warning("get modified err, skip model load, name: %s, path: %s, tm: %d", name.c_str(), path.c_str(), tm);
      return;
    }
    auto it = models_.find(name);
    if (it == models_.end() || tm > it->second->last_modified_) {
      Boosting *new_model = nullptr;
      try {
        new_model = Boosting::CreateBoosting("gbdt", path.c_str());
      } catch (const std::runtime_error &error) {
        Log::Warning("load model error, name: %s, path: %s, tm: %d, err: %s",
                     name.c_str(),
                     path.c_str(),
                     tm,
                     error.what());
        return;
      }
      if (new_model == nullptr) {
        Log::Warning("load model null, name: %s, path: %s, tm: %d", name.c_str(), path.c_str(), tm);
        return;
      }
      models_[name] = std::make_shared<ModelContainer>(new_model, name, path, tm);
      Log::Info("load model %s from %s, tm: %d", name.c_str(), path.c_str(), tm);
    }
  }

  void StartWatcher() {
    std::thread t(&ServiceImpl::ModelWatcher, this);
    t.detach();
  }

  void ModelWatcher() {
    auto last_modified = VirtualFileStat::GetLastModified(model_config_);
    while (run_) {
      sleep(30);
      auto tm = VirtualFileStat::GetLastModified(model_config_);
      if (tm > last_modified && ReloadModelConfigFile()) {
        last_modified = tm;
        continue;
      }
      for (auto &item: models_) {
        ReloadModel(item.first, item.second->path_);
      }
    }
  }

  std::shared_ptr<ModelContainer> GetModel(const std::string &name) {
    AutoRead(&this->rw_lock_);
    auto it = models_.find(name);
    if (it == models_.end()) {
      return std::shared_ptr<ModelContainer>(nullptr);
    }
    return it->second;
  }

  RWLock rw_lock_;
  std::unordered_map<std::string, std::shared_ptr<ModelContainer>> models_;
  std::string model_config_;
  bool run_;
};

}  // namespace LightGBM
#endif

#endif   // LIGHTGBM_ServiceImpl_HPP_
