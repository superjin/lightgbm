#include <LightGBM/utils/file_io.h>
#include <LightGBM/utils/log.h>

#include <algorithm>
#include <unistd.h>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unordered_map>
#ifdef USE_HDFS
#include <hdfs.h>
#endif

#ifdef USE_S3
#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/core/utils/FileSystemUtils.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/HeadObjectRequest.h>
#include <aws/s3/model/HeadObjectResult.h>
#endif

namespace LightGBM {

struct LocalFile : VirtualFileReader, VirtualFileWriter, VirtualFileStat {
  LocalFile(const std::string &filename, const std::string &mode) : filename_(filename), mode_(mode) {}
  virtual ~LocalFile() {
    if (file_ != NULL) {
      fclose(file_);
    }
  }

  bool Init() {
    if (file_ == NULL) {
#if _MSC_VER
      fopen_s(&file_, filename_.c_str(), mode_.c_str());
#else
      file_ = fopen(filename_.c_str(), mode_.c_str());
#endif
    }
    return file_ != NULL;
  }

  bool Exists() const {
    LocalFile file(filename_, "rb");
    return file.Init();
  }

  size_t Read(void *buffer, size_t bytes) const {
    return fread(buffer, 1, bytes, file_);
  }

  size_t Write(const void *buffer, size_t bytes) const {
    return fwrite(buffer, bytes, 1, file_) == 1 ? bytes : 0;
  }

  long GetLastModified() {
    Init();
    if (file_ != NULL) {
      struct stat buf;
      int fd = fileno(file_);
      fstat(fd, &buf);
      return buf.st_mtime;
    }
    return -1;
  }

 private:
  FILE *file_ = NULL;
  const std::string filename_;
  const std::string mode_;
};

const std::string kHdfsProto = "hdfs://";

#ifdef USE_HDFS
struct HDFSFile : VirtualFileReader, VirtualFileWriter {
  HDFSFile(const std::string& filename, int flags) : filename_(filename), flags_(flags) {}
  ~HDFSFile() {
    if (file_ != NULL) {
      hdfsCloseFile(fs_, file_);
    }
  }

  bool Init() {
    if (file_ == NULL) {
      if (fs_ == NULL) {
        fs_ = GetHDFSFileSystem(filename_);
      }
      if (fs_ != NULL && (flags_ == O_WRONLY || 0 == hdfsExists(fs_, filename_.c_str()))) {
        file_ = hdfsOpenFile(fs_, filename_.c_str(), flags_, 0, 0, 0);
      }
    }
    return file_ != NULL;
  }

  bool Exists() const {
    if (fs_ == NULL) {
      fs_ = GetHDFSFileSystem(filename_);
    }
    return fs_ != NULL && 0 == hdfsExists(fs_, filename_.c_str());
  }

  size_t Read(void* data, size_t bytes) const {
    return FileOperation<void*>(data, bytes, &hdfsRead);
  }

  size_t Write(const void* data, size_t bytes) const {
    return FileOperation<const void*>(data, bytes, &hdfsWrite);
  }

private:
  template <typename BufferType>
  using fileOp = tSize(*)(hdfsFS, hdfsFile, BufferType, tSize);

  template <typename BufferType>
  inline size_t FileOperation(BufferType data, size_t bytes, fileOp<BufferType> op) const {
    char* buffer = (char *)data;
    size_t remain = bytes;
    while (remain != 0) {
      size_t nmax = static_cast<size_t>(std::numeric_limits<tSize>::max());
      tSize ret = op(fs_, file_, buffer, std::min(nmax, remain));
      if (ret > 0) {
        size_t n = static_cast<size_t>(ret);
        remain -= n;
        buffer += n;
      } else if (ret == 0) {
        break;
      } else if (errno != EINTR) {
        Log::Fatal("Failed HDFS file operation [%s]", strerror(errno));
      }
    }
    return bytes - remain;
  }

  static hdfsFS GetHDFSFileSystem(const std::string& uri) {
    size_t end = uri.find("/", kHdfsProto.length());
    if (uri.find(kHdfsProto) != 0 || end == std::string::npos) {
      Log::Warning("Bad HDFS uri, no namenode found [%s]", uri.c_str());
      return NULL;
    }
    std::string hostport = uri.substr(kHdfsProto.length(), end - kHdfsProto.length());
    if (fs_cache_.count(hostport) == 0) {
      fs_cache_[hostport] = MakeHDFSFileSystem(hostport);
    }
    return fs_cache_[hostport];
  }

  static hdfsFS MakeHDFSFileSystem(const std::string& hostport) {
    std::istringstream iss(hostport);
    std::string host;
    tPort port = 0;
    std::getline(iss, host, ':');
    iss >> port;
    hdfsFS fs = iss.eof() ? hdfsConnect(host.c_str(), port) : NULL;
    if (fs == NULL) {
      Log::Warning("Could not connect to HDFS namenode [%s]", hostport.c_str());
    }
    return fs;
  }

  mutable hdfsFS fs_ = NULL;
  hdfsFile file_ = NULL;
  const std::string filename_;
  const int flags_;
  static std::unordered_map<std::string, hdfsFS> fs_cache_;
};

std::unordered_map<std::string, hdfsFS> HDFSFile::fs_cache_ = std::unordered_map<std::string, hdfsFS>();

#define WITH_HDFS(x) x
#else
#define WITH_HDFS(x) Log::Fatal("HDFS support is not enabled")
#endif // USE_HDFS

const std::string kS3Proto = "s3://";

#ifdef USE_S3

struct S3File : VirtualFileReader, VirtualFileWriter, VirtualFileStat {
  S3File(const std::string &filename, const std::string &mode) :
      filename_(filename),
      mode_(mode),
      buffer_read_left_(0),
      buffer_read_total_(0),
      read_offset_(0),
      max_size_(0),
      buffer_size_(64 * 1024 * 1024),
      outfile_(std::make_shared<Aws::Utils::TempFile>("/tmp/s3_filesystem_XXXXXX",
                                                      std::ios_base::binary | std::ios_base::trunc | std::ios_base::in
                                                          | std::ios_base::out)),
      need_sync_(false) {
    Aws::Client::ClientConfiguration clientConfig;
    clientConfig.connectTimeoutMs = 30000;
    clientConfig.requestTimeoutMs = 60000;
    auto region = std::string(std::getenv("S3_REGION"));
    if (!region.empty()) {
      clientConfig.region = std::getenv("S3_REGION");
    } else {
      Log::Info("env S3_REGION not found");
    }
    s3_client_ = Aws::S3::S3Client(clientConfig);

    size_t end = filename_.find("/", kS3Proto.length());
    if (filename_.find(kS3Proto) != 0 || end == std::string::npos) {
      Log::Warning("Bad s3 uri, no bucket found [%s]", filename_.c_str());
    }
    s3_bucket_ = filename_.substr(kS3Proto.length(), end - kS3Proto.length());
    s3_object_ = filename_.substr(end + 1, filename_.length());
  }
  ~S3File() override {
    if (need_sync_) {
      Sync();
    }
  }

  bool Init() override {
    if (mode_ == "r") {
      max_size_ = GetFileSize();
      buffer_size_ = std::min(buffer_size_, max_size_);
      buffer_read_.resize(buffer_size_);
      return max_size_ > 0;
    }
    return true;
  }

  bool Exists() const {
    S3File file(filename_, "r");
    return file.Init();
  }

  size_t Read(void *data, size_t bytes) const override {
    size_t read_size = 0;
    while (read_size < bytes) {
      if (buffer_read_left_ == 0) {
        buffer_read_total_ = ReadBlock(buffer_read_.data(), read_offset_, buffer_size_);
        if (buffer_read_total_ == 0)
          break;
        read_offset_ += buffer_read_total_;
        buffer_read_left_ = buffer_read_total_;
      }
      auto copy_size = std::min(bytes - read_size, buffer_read_left_);

      std::memcpy((char *) data + read_size, buffer_read_.data() + (buffer_read_total_ - buffer_read_left_), copy_size);
      buffer_read_left_ -= copy_size;
      read_size += copy_size;
    }
    return read_size;
  }

  size_t Write(const void *data, size_t bytes) const override {
    if (!outfile_) {
      return 0;
    }
    need_sync_ = true;
    outfile_->write((char *) data, bytes);
    if (!outfile_->good()) {
      return 0;
    }
    return bytes;
  }

  long GetLastModified() {
    auto response = GetHeadObjectResponse();
    if (response.IsSuccess()) {
      return response.GetResult().GetLastModified().Millis() / 1000;
    }
    return -1;
  }

 private:
  Aws::S3::Model::HeadObjectOutcome GetHeadObjectResponse() const {
    int retry_count = 5;
    Aws::S3::Model::HeadObjectRequest object_request;
    object_request.WithBucket(s3_bucket_.c_str()).WithKey(s3_object_.c_str());
    Aws::S3::Model::HeadObjectOutcome response;

    do {
      retry_count--;
      response = s3_client_.HeadObject(object_request);
      if (response.IsSuccess()) {
        return response;
      }
      sleep(3);
    } while (retry_count >= 0);

    return response;
  }

  size_t GetFileSize() const {
    auto response = GetHeadObjectResponse();
    if (response.IsSuccess()) {
      return size_t(response.GetResult().GetContentLength());
    }
    Log::Info(response.GetError().GetMessage().data());
    return 0;
  }

  size_t ReadBlock(void *data, size_t start_offset, size_t bytes) const {
    if (start_offset > max_size_ - 1) {
      return 0;
    }
    int retry_count = 5;
    Aws::S3::Model::GetObjectRequest object_request;
    object_request.WithBucket(s3_bucket_.c_str()).WithKey(s3_object_.c_str());
    auto end_offset = std::min(start_offset + bytes - 1, max_size_ - 1);

    std::string range = "bytes=" + std::to_string(start_offset) + "-" + std::to_string(end_offset);
    object_request.SetRange(range.c_str());

    do {
      retry_count--;
      auto response = s3_client_.GetObject(object_request);

      if (response.IsSuccess()) {
        auto length = response.GetResult().GetContentLength();
        std::stringstream ss;
        ss << response.GetResult().GetBody().rdbuf();
        ss.read((char *) data, length);
        return size_t(length);
      }
      Log::Info(response.GetError().GetMessage().data());
      sleep(3);
    } while (retry_count >= 0);

    return 0;
  }

  void Sync() {
    int retry_count = 5;
    Aws::S3::Model::PutObjectRequest putObjectRequest;
    putObjectRequest.WithBucket(s3_bucket_.c_str()).WithKey(s3_object_.c_str());
    long offset = outfile_->tellp();
    outfile_->seekg(0);

    do {
      retry_count--;
      putObjectRequest.SetBody(outfile_);
      putObjectRequest.SetContentLength(offset);
      auto response = s3_client_.PutObject(putObjectRequest);
      if (response.IsSuccess()) {
        break;
      }
      Log::Info(response.GetError().GetMessage().data());
      sleep(3);
    } while (retry_count >= 0);

    outfile_->clear();
    outfile_->seekp(offset);
  }

  const std::string filename_;
  const std::string mode_;

  Aws::S3::S3Client s3_client_;
  std::string s3_bucket_;
  std::string s3_object_;

  mutable std::vector<char> buffer_read_;
  mutable size_t buffer_read_left_;
  mutable size_t buffer_read_total_;
  mutable size_t read_offset_;

  size_t max_size_;
  size_t buffer_size_;

  std::shared_ptr<Aws::Utils::TempFile> outfile_;
  mutable bool need_sync_;

};

#define WITH_S3(x) x
#else
#define WITH_S3(x) Log::Fatal("s3 support is not enabled")
#endif // USE_S3

std::unique_ptr<VirtualFileReader> VirtualFileReader::Make(const std::string &filename) {
  if (0 == filename.find(kHdfsProto)) {
    WITH_HDFS(return std::unique_ptr<VirtualFileReader>(new HDFSFile(filename, O_RDONLY)));
  } else if (0 == filename.find(kS3Proto)) {
    WITH_S3(return std::unique_ptr<VirtualFileReader>(new S3File(filename, "r")));
  } else {
    return std::unique_ptr<VirtualFileReader>(new LocalFile(filename, "rb"));
  }
}

std::unique_ptr<VirtualFileWriter> VirtualFileWriter::Make(const std::string &filename) {
  if (0 == filename.find(kHdfsProto)) {
    WITH_HDFS(return std::unique_ptr<VirtualFileWriter>(new HDFSFile(filename, O_WRONLY)));
  } else if (0 == filename.find(kS3Proto)) {
    WITH_S3(return std::unique_ptr<VirtualFileWriter>(new S3File(filename, "w")));
  } else {
    return std::unique_ptr<VirtualFileWriter>(new LocalFile(filename, "wb"));
  }
}

bool VirtualFileWriter::Exists(const std::string &filename) {
  if (0 == filename.find(kHdfsProto)) {
    WITH_HDFS(HDFSFile file(filename, O_RDONLY);
                  return file.Exists());
  } else if (0 == filename.find(kS3Proto)) {
    WITH_S3(S3File file(filename, "r");
                return file.Exists());
  } else {
    LocalFile file(filename, "rb");
    return file.Exists();
  }
}

long VirtualFileStat::GetLastModified(const std::string &filename) {
  if (0 == filename.find(kS3Proto)) {
    WITH_S3(S3File file(filename, "r");
                return file.GetLastModified());
  } else {
    LocalFile file(filename, "rb");
    return file.GetLastModified();
  }
}

}  // namespace LightGBM
